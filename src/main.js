import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import { routes } from './routes'
import i18n from './lang/i18n'
import { store } from './store/store'
import FlagIcon from 'vue-flag-icon';

Vue.config.productionTip = false

Vue.use(VueRouter)
const router = new VueRouter({
  mode: 'history',
  routes
});

Vue.use(FlagIcon);

new Vue({
  router,
  i18n,
  store,
  render: h => h(App),
}).$mount('#app')
