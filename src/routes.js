import NotFound from './components/404.vue'
import UserList from './components/UserList.vue'
import UserDetail from './components/UserDetail.vue'
import User from './components/User.vue'
import UserCreation from './components/UserCreation.vue'

export const routes = [
  {
    path: '/', redirect: '/user'
  },
  {
    path: '/user',
    component: User,
    children: [
      { path: '', name: 'user-list', component: UserList },
      { path: ':id', name: 'user-detail', component: UserDetail },
      { path: 'new', name: 'user-new', component: UserCreation }
    ],
  },
  {
    path: '/404', component: NotFound
  },
  {
    path: '*', redirect: '/404'
  },
];